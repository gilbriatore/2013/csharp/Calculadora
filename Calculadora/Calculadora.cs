﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using Calculadora;

namespace Calculadora 
{
    class Calculadora : Form
    {
        private Label display;
        private Panel painelDeTeclas;
        private Button btnVazio;
        private Button btnDividir;
        private Button btnMultiplicar;
        private Button btnCalcular;
        private Button btnTres;
        private Button btnUm;
        private Button btnDois;
        private Button btnVazio2;
        private Button btnSeis;
        private Button btnQuatro;
        private Button btnCinco;
        private Button btnSomar;
        private Button btnNove;
        private Button btnSete;
        private Button btnOito;
        private Button btnVirgula;
        private Button btnZero;
        private Button btnSubtrair;

        Calculadora()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.painelDeTeclas = new System.Windows.Forms.Panel();
            this.btnVirgula = new System.Windows.Forms.Button();
            this.btnZero = new System.Windows.Forms.Button();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.btnTres = new System.Windows.Forms.Button();
            this.btnUm = new System.Windows.Forms.Button();
            this.btnDois = new System.Windows.Forms.Button();
            this.btnVazio2 = new System.Windows.Forms.Button();
            this.btnSeis = new System.Windows.Forms.Button();
            this.btnQuatro = new System.Windows.Forms.Button();
            this.btnCinco = new System.Windows.Forms.Button();
            this.btnSomar = new System.Windows.Forms.Button();
            this.btnNove = new System.Windows.Forms.Button();
            this.btnSete = new System.Windows.Forms.Button();
            this.btnOito = new System.Windows.Forms.Button();
            this.btnSubtrair = new System.Windows.Forms.Button();
            this.btnMultiplicar = new System.Windows.Forms.Button();
            this.btnVazio = new System.Windows.Forms.Button();
            this.btnDividir = new System.Windows.Forms.Button();
            this.display = new System.Windows.Forms.Label();
            this.painelDeTeclas.SuspendLayout();
            this.SuspendLayout();
            // 
            // painelDeTeclas
            // 
            this.painelDeTeclas.Controls.Add(this.btnVirgula);
            this.painelDeTeclas.Controls.Add(this.btnZero);
            this.painelDeTeclas.Controls.Add(this.btnCalcular);
            this.painelDeTeclas.Controls.Add(this.btnTres);
            this.painelDeTeclas.Controls.Add(this.btnUm);
            this.painelDeTeclas.Controls.Add(this.btnDois);
            this.painelDeTeclas.Controls.Add(this.btnVazio2);
            this.painelDeTeclas.Controls.Add(this.btnSeis);
            this.painelDeTeclas.Controls.Add(this.btnQuatro);
            this.painelDeTeclas.Controls.Add(this.btnCinco);
            this.painelDeTeclas.Controls.Add(this.btnSomar);
            this.painelDeTeclas.Controls.Add(this.btnNove);
            this.painelDeTeclas.Controls.Add(this.btnSete);
            this.painelDeTeclas.Controls.Add(this.btnOito);
            this.painelDeTeclas.Controls.Add(this.btnSubtrair);
            this.painelDeTeclas.Controls.Add(this.btnMultiplicar);
            this.painelDeTeclas.Controls.Add(this.btnVazio);
            this.painelDeTeclas.Controls.Add(this.btnDividir);
            this.painelDeTeclas.Location = new System.Drawing.Point(20, 89);
            this.painelDeTeclas.Name = "painelDeTeclas";
            this.painelDeTeclas.Size = new System.Drawing.Size(200, 179);
            this.painelDeTeclas.TabIndex = 1;
            // 
            // btnVirgula
            // 
            this.btnVirgula.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVirgula.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnVirgula.Location = new System.Drawing.Point(100, 144);
            this.btnVirgula.Name = "btnVirgula";
            this.btnVirgula.Size = new System.Drawing.Size(45, 30);
            this.btnVirgula.TabIndex = 16;
            this.btnVirgula.Text = ",";
            this.btnVirgula.UseVisualStyleBackColor = true;
            // 
            // btnZero
            // 
            this.btnZero.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZero.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnZero.Location = new System.Drawing.Point(4, 144);
            this.btnZero.Name = "btnZero";
            this.btnZero.Size = new System.Drawing.Size(90, 30);
            this.btnZero.TabIndex = 15;
            this.btnZero.Text = "0";
            this.btnZero.UseVisualStyleBackColor = true;
            // 
            // btnCalcular
            // 
            this.btnCalcular.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalcular.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnCalcular.Location = new System.Drawing.Point(147, 108);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(45, 66);
            this.btnCalcular.TabIndex = 14;
            this.btnCalcular.Text = "=";
            this.btnCalcular.UseVisualStyleBackColor = true;
            // 
            // btnTres
            // 
            this.btnTres.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTres.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnTres.Location = new System.Drawing.Point(99, 108);
            this.btnTres.Name = "btnTres";
            this.btnTres.Size = new System.Drawing.Size(45, 30);
            this.btnTres.TabIndex = 13;
            this.btnTres.Text = "3";
            this.btnTres.UseVisualStyleBackColor = true;
            // 
            // btnUm
            // 
            this.btnUm.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUm.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnUm.Location = new System.Drawing.Point(3, 108);
            this.btnUm.Name = "btnUm";
            this.btnUm.Size = new System.Drawing.Size(45, 30);
            this.btnUm.TabIndex = 11;
            this.btnUm.Text = " 1";
            this.btnUm.UseVisualStyleBackColor = true;
            // 
            // btnDois
            // 
            this.btnDois.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDois.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnDois.Location = new System.Drawing.Point(51, 108);
            this.btnDois.Name = "btnDois";
            this.btnDois.Size = new System.Drawing.Size(45, 30);
            this.btnDois.TabIndex = 12;
            this.btnDois.Text = "2";
            this.btnDois.UseVisualStyleBackColor = true;
            // 
            // btnVazio2
            // 
            this.btnVazio2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVazio2.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnVazio2.Location = new System.Drawing.Point(147, 72);
            this.btnVazio2.Name = "btnVazio2";
            this.btnVazio2.Size = new System.Drawing.Size(45, 30);
            this.btnVazio2.TabIndex = 10;
            this.btnVazio2.UseVisualStyleBackColor = true;
            // 
            // btnSeis
            // 
            this.btnSeis.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeis.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnSeis.Location = new System.Drawing.Point(99, 72);
            this.btnSeis.Name = "btnSeis";
            this.btnSeis.Size = new System.Drawing.Size(45, 30);
            this.btnSeis.TabIndex = 9;
            this.btnSeis.Text = "6";
            this.btnSeis.UseVisualStyleBackColor = true;
            // 
            // btnQuatro
            // 
            this.btnQuatro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuatro.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnQuatro.Location = new System.Drawing.Point(3, 72);
            this.btnQuatro.Name = "btnQuatro";
            this.btnQuatro.Size = new System.Drawing.Size(45, 30);
            this.btnQuatro.TabIndex = 7;
            this.btnQuatro.Text = " 4";
            this.btnQuatro.UseVisualStyleBackColor = true;
            // 
            // btnCinco
            // 
            this.btnCinco.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCinco.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnCinco.Location = new System.Drawing.Point(51, 72);
            this.btnCinco.Name = "btnCinco";
            this.btnCinco.Size = new System.Drawing.Size(45, 30);
            this.btnCinco.TabIndex = 8;
            this.btnCinco.Text = "5";
            this.btnCinco.UseVisualStyleBackColor = true;
            // 
            // btnSomar
            // 
            this.btnSomar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSomar.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnSomar.Location = new System.Drawing.Point(148, 36);
            this.btnSomar.Name = "btnSomar";
            this.btnSomar.Size = new System.Drawing.Size(45, 30);
            this.btnSomar.TabIndex = 6;
            this.btnSomar.Text = "+";
            this.btnSomar.UseVisualStyleBackColor = true;
            // 
            // btnNove
            // 
            this.btnNove.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNove.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnNove.Location = new System.Drawing.Point(100, 36);
            this.btnNove.Name = "btnNove";
            this.btnNove.Size = new System.Drawing.Size(45, 30);
            this.btnNove.TabIndex = 5;
            this.btnNove.Text = "9";
            this.btnNove.UseVisualStyleBackColor = true;
            // 
            // btnSete
            // 
            this.btnSete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSete.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnSete.Location = new System.Drawing.Point(4, 36);
            this.btnSete.Name = "btnSete";
            this.btnSete.Size = new System.Drawing.Size(45, 30);
            this.btnSete.TabIndex = 3;
            this.btnSete.Text = " 7";
            this.btnSete.UseVisualStyleBackColor = true;
            this.btnSete.Click += new System.EventHandler(this.btnSete_Click);
            // 
            // btnOito
            // 
            this.btnOito.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOito.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnOito.Location = new System.Drawing.Point(51, 36);
            this.btnOito.Name = "btnOito";
            this.btnOito.Size = new System.Drawing.Size(45, 30);
            this.btnOito.TabIndex = 4;
            this.btnOito.Text = "8";
            this.btnOito.UseVisualStyleBackColor = true;
            this.btnOito.Click += new System.EventHandler(this.btnOito_Click);
            // 
            // btnSubtrair
            // 
            this.btnSubtrair.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubtrair.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnSubtrair.Location = new System.Drawing.Point(147, 0);
            this.btnSubtrair.Name = "btnSubtrair";
            this.btnSubtrair.Size = new System.Drawing.Size(45, 30);
            this.btnSubtrair.TabIndex = 2;
            this.btnSubtrair.Text = "-";
            this.btnSubtrair.UseVisualStyleBackColor = true;
            // 
            // btnMultiplicar
            // 
            this.btnMultiplicar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMultiplicar.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnMultiplicar.Location = new System.Drawing.Point(99, 0);
            this.btnMultiplicar.Name = "btnMultiplicar";
            this.btnMultiplicar.Size = new System.Drawing.Size(45, 30);
            this.btnMultiplicar.TabIndex = 1;
            this.btnMultiplicar.Text = "*";
            this.btnMultiplicar.UseVisualStyleBackColor = true;
            // 
            // btnVazio
            // 
            this.btnVazio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVazio.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnVazio.Location = new System.Drawing.Point(3, 0);
            this.btnVazio.Name = "btnVazio";
            this.btnVazio.Size = new System.Drawing.Size(45, 30);
            this.btnVazio.TabIndex = 0;
            this.btnVazio.Text = " ";
            this.btnVazio.UseVisualStyleBackColor = true;
            // 
            // btnDividir
            // 
            this.btnDividir.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDividir.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnDividir.Location = new System.Drawing.Point(51, 0);
            this.btnDividir.Name = "btnDividir";
            this.btnDividir.Size = new System.Drawing.Size(45, 30);
            this.btnDividir.TabIndex = 0;
            this.btnDividir.Text = "/";
            this.btnDividir.UseVisualStyleBackColor = true;
            // 
            // display
            // 
            this.display.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.display.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.display.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.display.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.display.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.display.Location = new System.Drawing.Point(23, 23);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(190, 50);
            this.display.TabIndex = 2;
            this.display.Text = "0,0";
            this.display.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Calculadora
            // 
            this.ClientSize = new System.Drawing.Size(240, 280);
            this.Controls.Add(this.display);
            this.Controls.Add(this.painelDeTeclas);
            this.Name = "Calculadora";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculadora";
            this.painelDeTeclas.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #region Método Main
        [STAThread]
        static void Main(string[] s)
        {
            Application.EnableVisualStyles();
            Application.Run(new Calculadora());
        }
        #endregion

        private void btnSete_Click(object sender, EventArgs e)
        {
            this.display.Text = "7";
        }

        private void btnOito_Click(object sender, EventArgs e)
        {
            this.display.Text = "8";
        }
    }
}
